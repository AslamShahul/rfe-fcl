import { RFEPage } from './app.po';

describe('rfe App', () => {
  let page: RFEPage;

  beforeEach(() => {
    page = new RFEPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
