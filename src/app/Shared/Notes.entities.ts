

    export interface Metadata {
        id: string;
        uri: string;
        etag: string;
        type: string;
    }

    export interface Metadata2 {
        id: string;
        type: string;
    }

    export interface Author {
        __metadata: Metadata2;
        Id: number;
        Title: string;
    }

    export interface IResult {
        __metadata: Metadata;
        Author: Author;
        Comments: string;
        Created: Date;
        ParentId:number;
        Display:string;
        LeftMargin:string;
        ID:number;
    }

    export interface D {
        results: IResult[];
    }

    export interface RootObject {
        d: D;
    }



