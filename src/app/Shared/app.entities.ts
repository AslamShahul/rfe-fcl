export interface Metadata {
        id: string;
        uri: string;
        etag: string;
        type: string;
    }

    export interface Metadata2 {
        id: string;
        type: string;
    }

    export interface AssignedTo {
        __metadata: Metadata2;
        Id: number;
        Title: string;
    }

    export interface IResult {
        __metadata: Metadata;
        AssignedTo: AssignedTo;
        Id: number;
        Title: string;
        Status: string;
        RelatedItems: string;
        TaskOutcome?: any;
        Comments: string;
        Modified:Date;
        
        ID: number;
    }

    export interface D {
        results: IResult[];
    }

    export interface RootObject {
        d: D;
    }
     