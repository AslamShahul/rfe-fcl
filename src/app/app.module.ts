import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { RouterConfig ,appRoutingProviders } from './app.routes';
import { AppComponent } from './app.component';
import { TasksComponent } from './Tasks/Tasks.component';
import { TextBoxComponent } from './Shared/textbox.component';
import { ButtonComponent } from './Shared/Button.component';
import { CancelButtonComponent } from './Shared/CancelButton.component';
import { clickWf } from './Shared/directives';
import { CancelWf } from './Shared/Cancelworkflowdirective';
import { InitiateWorkflowComponent } from './InitiateWorkflow/InitiateWorkflow.component';
import { CancelWorkflowComponent } from './CancelWorkflow/CancelWorkflow.component';
import {CommentsComponent} from './Forms/Comments.component'
import {CommentsDetail} from './Forms/CommentsDetail.component'
import {PopupModule} from 'ng2-opd-popup'

import { AppSettings } from './Shared/app.settings';

@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    TextBoxComponent,
    ButtonComponent,
    InitiateWorkflowComponent,
    clickWf,CommentsComponent,CommentsDetail,CancelButtonComponent,CancelWf,CancelWorkflowComponent
  ],
  imports: [
        BrowserModule,
        RouterModule.forRoot(RouterConfig),
        HttpModule,
        FormsModule,ReactiveFormsModule,PopupModule.forRoot(),

  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy },
        AppSettings,appRoutingProviders],
  bootstrap: [TasksComponent,CommentsComponent,CommentsComponent]
})
export class AppModule { }
